## How to use:
Clone this repo in: <b>
vendor/xiaomi/miuicamera <b> 

Inherit in device tree with: <b>
$(call inherit-product-if-exists, vendor/xiaomi/miuicamera/config.mk) <b>

Add prop: <b>
persist.vendor.camera.privapp.list=com.android.camera
